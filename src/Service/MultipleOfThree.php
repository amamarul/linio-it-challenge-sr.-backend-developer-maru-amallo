<?php
namespace App\Service;

class MultipleOfThree extends MultipleOf
{
    public function __construct() {
        $this->setMultiple(3);
    }
}
