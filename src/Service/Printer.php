<?php
namespace App\Service;

use App\Contract\PrintInterface;

class Printer implements PrintInterface
{
    private $value;
    private $line_separator;

    public function __construct($line_separator = null)
    {
        $this->line_separator = $line_separator;
    }
 
    public function setValue($value)
    {
        $this->value = $value;
    }
 
    public function getValue()
    {
        return $this->value;
    }
 
    public function setLineSeparator($line_separator)
    {
        $this->line_separator = $line_separator;
    }
 
    public function getLineSeparator()
    {
        return $this->line_separator;
    }
 
    public function print()
    {
        echo $this->value . $this->line_separator;
    }
}
