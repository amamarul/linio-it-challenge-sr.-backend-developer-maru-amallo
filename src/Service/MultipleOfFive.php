<?php
namespace App\Service;

class MultipleOfFive extends MultipleOf
{ 
    public function __construct() {
        $this->setMultiple(5);
    }
}
