<?php
namespace App\Service;

use App\Contract\MultipleOfInterface;

class MultipleOf implements MultipleOfInterface
{
    private $value;
    private $multiple;

    public function __construct($value = null, $multiple = null) {
        $this->value = $value;
        $this->multiple = $multiple;
    }
 
    public function setValue($value)
    {
        $this->value = $value;
    }
 
    public function getValue()
    {
        return $this->value;
    }
 
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }
 
    public function getMultiple()
    {
        return $this->multiple;
    }
 
    public function isMultiple()
    {
        return $this->value % $this->multiple == 0;
    }

    public function run($value)
    {
        $this->setValue($value);

        return $this->isMultiple();
    }
}
