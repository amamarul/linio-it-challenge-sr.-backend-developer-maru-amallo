<?php
namespace App;

use App\Service\Printer;
use App\Service\MultipleOfFive;
use App\Contract\LinioInterface;
use App\Service\MultipleOfThree;

class Linio implements LinioInterface
{
    private $from;
    private $to;

    public function __construct($from = null, $to = null) {
        $this->from = $from;
        $this->to = $to;
    }
 
    public function setValueFrom($from)
    {
        $this->from = $from;
    }
 
    public function getValueFrom()
    {
        return $this->from;
    }
 
    public function setValueTo($to)
    {
        $this->to = $to;
    }
 
    public function getValueTo()
    {
        return $this->to;
    }
 
    public function getNumberOrText($number)
    {
        $multipleOfThree = new MultipleOfThree;
        $multipleOfFive = new MultipleOfFive;

        $result = '';

        switch (true) {
            case $multipleOfThree->run($number) && $multipleOfFive->run($number):
                $result = 'Linianos';
                break;
            case $multipleOfThree->run($number):
                $result = 'Linio';
                break; 
            case $multipleOfFive->run($number):
                $result = 'IT';
                break; 
            default: 
                $result = $number;
                break;
        }

        return $result;
    }

    public function getSeparator()
    {
        /* If script runs into command line use \n otherwise br */
        $separator = '<br>';

        if (defined('STDIN')) {
            return $separator = "\n";
        }

        return $separator;
    }
 
    public function run() : void
    {
        $separator = $this->getSeparator();

        foreach (range($this->from, $this->to) as $key => $number)
        {
            $value = $this->getNumberOrText($number);

            $printer = new Printer($separator);
            $printer->setValue($value);
            $printer->print();
        }

    }
}
