<?php
namespace App\Contract;

interface MultipleOfInterface
{
    public function setValue($value);
    public function getValue();
    public function setMultiple($multiple);
    public function getMultiple();
    public function isMultiple();
    public function run($value);
}