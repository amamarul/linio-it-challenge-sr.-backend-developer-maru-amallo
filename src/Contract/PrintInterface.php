<?php
namespace App\Contract;

interface PrintInterface
{
    public function setValue($value);
    public function getValue();
    public function setLineSeparator($line_separator);
    public function getLineSeparator();
    public function print();
}