<?php
namespace App\Contract;

interface LinioInterface
{
    public function setValueFrom($from);
    public function getValueFrom();
    public function setValueTo($to);
    public function getValueTo();
    public function getNumberOrText($number);
    public function getSeparator();
    public function run();
}