# Linio
## Linio IT

# Challenge – Sr. Backend Developer  
Write a program that prints all the numbers from 1 to 100. However, for
multiples of 3, instead of the number, print "Linio". For multiples of 5 print
"IT". For numbers which are multiples of both 3 and 5, print "Linianos".
But here's the catch: you can use only one `if`. No multiple branches, ternary
operators or `else`.

## Requirements  
* 1 `if`
* You can't use `else`, `else if` or ternary
* Unit tests
* Feel free to apply your SOLID knowledge
* You can write the challenge in any language you want. Here at Linio we are
big fans of PHP, Kotlin and TypeScript

## Submission
Create a repository on GitLab, GitHub or any other similar service and just send us the link.


# Install

1. clone this repo      
1. run `composer install`  

# Testing

run `php vendor/bin/phpunit --testdox`  

# Usage

### **At browser:**   
1. run `php -S localhost:8000`  
2. Open browser and go to `localhost:8000`  

### **At Command Line:**   
run `php index.php`  

# Extending  

You can extend Linio functionalities changing only the criteria for displaying values

## Example  

```php
    <?php
    namespace App;

    use App\Linio;

    class OtherLinio extends Linio
    { 
        public function getNumberOrText($number)
        {
            // Another implementation 
            return '';
        }
    }
```

# TODO

- [ ] Comment all methods and classes
- [ ] Use as a Php Package (`composer require maruamallo/linio-challenge`)

# Extra
This package is an implementation of the following script  

```php
    // linio-challenge.php
    <?php 

    function isMultipleOf($value, $multiple)
    {
        return $value % $multiple == 0;
    }

    function printText($number)
    {
        switch (true) {
            case isMultipleOf($number,3) && isMultipleOf($number, 5):
                echo 'Linianos' . "\n";
                break;
            case isMultipleOf($number, 3):
                echo 'Linio' . "\n";
                break; 
            case isMultipleOf($number, 5):
                echo 'IT' . "\n";
                break; 
            default: 
                echo $number . "\n";
                break;
        }
    }

    foreach (range(1,100) as $number) {
        printText($number);
    }

```