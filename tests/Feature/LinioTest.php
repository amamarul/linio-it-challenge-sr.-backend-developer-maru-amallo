<?php
namespace Tests\Feature;

use PHPUnit\Framework\TestCase;
use App\Linio;

final class LinioTest extends TestCase
{
    /** @test */
    public function linio_class_10_to_50(): void
    {
        ob_start();
        $output = (new Linio(10,50))->run();
        $actualOutput = ob_get_clean();
        $this->assertStringContainsString('Linio',$actualOutput);
        $this->assertStringContainsString('Linianos',$actualOutput);
        $this->assertStringContainsString('IT',$actualOutput);
    }

    /** @test */
    public function linio_class_1_to_10(): void
    {
        ob_start();
        $output = (new Linio(1,10))->run();
        $actualOutput = ob_get_clean();
        $this->assertStringContainsString('Linio',$actualOutput);
        $this->assertStringNotContainsString('Linianos',$actualOutput);
        $this->assertStringContainsString('IT',$actualOutput);
    }
}
