<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Service\MultipleOfFive;

final class MultipleOfFiveTest extends TestCase
{
    /** @test */
    public function run_method_return_boolean_value_in_multipleOfFive_class(): void
    {
        $multiple = new MultipleOfFive;

        $this->assertTrue( is_bool($multiple->run(5)) );
    }
}
