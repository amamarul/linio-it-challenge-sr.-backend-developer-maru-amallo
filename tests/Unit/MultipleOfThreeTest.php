<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Service\MultipleOfThree;

final class MultipleOfThreeTest extends TestCase
{
    /** @test */
    public function run_method_return_boolean_value_in_multipleOfThree_class(): void
    {
        $multiple = new MultipleOfThree;

        $this->assertTrue( is_bool($multiple->run(3)) );
    }
}
