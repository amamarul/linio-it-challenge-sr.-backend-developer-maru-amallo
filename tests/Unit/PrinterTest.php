<?php
namespace Tests\Unit;

use App\Service\Printer;
use PHPUnit\Framework\TestCase;

final class PrinterTest extends TestCase
{
    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->printer = new Printer;
        $this->text       = 'Some data to print';
        $this->separator  = "\n";
    }

    /** @test */
    public function it_can_set_value_in_Printer_class(): void
    {
        $this->printer->setValue($this->text);

        $this->assertInstanceOf( Printer::class, $this->printer );
    }

    /** @test */
    public function it_can_get_value_in_Printer_class(): void
    {
        $this->printer->setValue($this->text);

        $this->assertEquals( $this->text, $this->printer->getValue() );
    }

    /** @test */
    public function it_can_set_separator_in_Printer_class(): void
    {
        $this->printer->setLineSeparator($this->separator);

        $this->assertInstanceOf( Printer::class, $this->printer );
    }

    /** @test */
    public function it_can_get_separator_in_Printer_class(): void
    {
        $this->printer->setLineSeparator($this->separator);

        $this->assertEquals( $this->separator, $this->printer->getLineSeparator() );
    }

    /** @test */
    public function print_method_return_value_with_endlineSeparator_in_Printer_class(): void
    {
        $this->printer->setValue($this->text);
        $this->printer->setLineSeparator($this->separator);

        ob_start();
        $this->printer->print();
        $actualOutput = ob_get_clean();
        $this->assertEquals( $this->text . $this->separator, $actualOutput );
    }
}
