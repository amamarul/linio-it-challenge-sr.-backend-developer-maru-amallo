<?php
namespace Tests\Unit;

use App\Service\MultipleOf;
use PHPUnit\Framework\TestCase;

final class MultipleOfTest extends TestCase
{
    /** @test */
    public function it_can_set_value_in_multipleOf_class(): void
    {
        $multiple = new MultipleOf;
        $multiple->setValue(10);

        $this->assertInstanceOf( MultipleOf::class, $multiple );
    }

    /** @test */
    public function it_can_get_value_in_multipleOf_class(): void
    {
        $multiple = new MultipleOf;
        $multiple->setValue(10);

        $this->assertEquals( 10, $multiple->getValue() );
    }

    /** @test */
    public function it_can_set_multiple_in_multipleOf_class(): void
    {
        $multiple = new MultipleOf;
        $multiple->setMultiple(5);

        $this->assertInstanceOf( MultipleOf::class, $multiple );
    }

    /** @test */
    public function it_can_get_multiple_in_multipleOf_class(): void
    {
        $multiple = new MultipleOf;
        $multiple->setMultiple(5);

        $this->assertEquals( 5, $multiple->getMultiple() );
    }

    /** @test */
    public function it_can_set_values_at_construct_in_multipleOf_class(): void
    {
        $multiple = new MultipleOf(10,5);

        $this->assertEquals( 10, $multiple->getValue() );
        $this->assertEquals( 5, $multiple->getMultiple() );
    }

    /** @test */
    public function isMultiple_method_return_boolean_value_in_multipleOf_class(): void
    {
        $multiple = new MultipleOf;
        $multiple->setValue(10);
        $multiple->setMultiple(5);

        $this->assertTrue( is_bool($multiple->isMultiple()) );
    }
}
